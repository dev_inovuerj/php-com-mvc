<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 19:47
 */
namespace App\Controllers;

class IndexController
{

    public function index()
    {
        echo "Route '/' | Controller : 'IndexController'";
    }

    public function contact()
    {

        echo "Route '/contact' | Controller : 'ContactController'";

    }
}