<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 19:47
 */
namespace App\Controllers;

class IndexController
{

    public function index()
    {
        include_once "../App/Views/index/index.phtml";
    }

    public function contact()
    {
        include_once "../App/Views/index/contact.phtml";
    }
}