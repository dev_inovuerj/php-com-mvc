<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 19:47
 */
namespace App\Controllers;

use App\Conn;
use App\Models\Client;
use SON\Controller\Action;

class IndexController extends Action
{

    public function index()
    {
        $client = new Client(Conn::getDB());

        $this->view->clientes = $client->fetchAll();

        $this->render("index");

    }

    public function contact()
    {
//        include_once "../App/Views/index/contact.phtml";

        $this->view->cars = ['Contact', 'Mustang', "Ferrari"];
        $this->render("contact", false);

    }

}