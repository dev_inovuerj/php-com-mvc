<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 21:32
 */

namespace App;


class Conn
{
    public static function getDB()
    {
        return new \PDO("mysql:host=localhost;dbname=test", "root", "1234",
            /* Working UTF-8 MySQL Built connection */
            array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );
    }
}