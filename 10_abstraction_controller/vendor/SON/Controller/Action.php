<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 20:57
 */

namespace SON\Controller;


Abstract class Action
{

    protected $view;
    private $action;

    public function __construct()
    {

        $this->view = new \stdClass();

    }

    protected function render($action, $layout = true)
    {
        $this->action = $action;

        if ($layout && file_exists('../App/Views/layout.phtml')) {

            echo "1";
            include_once "../App/Views/layout.phtml";

        } else {
            echo "2";

            $this->content();

        }
    }

    protected function content()
    {

        $current = get_class($this);

        $singleClassName = strtolower(str_replace("Controller", '', str_replace("App\\Controllers\\", '', $current)));

        include_once "../App/Views/" . $singleClassName . '/' . $this->action . '.phtml';

    }

}