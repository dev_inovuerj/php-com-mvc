<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 19:47
 */
namespace App\Controllers;

class IndexController
{

    private $view;

    public function __construct()
    {
        $this->view = new \stdClass();
    }

    public function index()
    {
//        include_once "../App/Views/index/index.phtml";

        $this->view->cars = ['Index', 'Mustang', "Ferrari"];
        $this->render("index");

    }

    public function contact()
    {
//        include_once "../App/Views/index/contact.phtml";

        $this->view->cars = ['Contact', 'Mustang', "Ferrari"];
        $this->render("contact");

    }

    public function render($action)
    {
        $current = get_class($this);

        $singleClassName = strtolower(str_replace("Controller", '', str_replace("App\\Controllers\\", '', $current)));

        include_once "../App/Views/" . $singleClassName . '/' . $action . '.phtml';


    }


}