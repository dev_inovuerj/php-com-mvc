<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 19:47
 */
namespace App\Controllers;

use SON\Controller\Action;

class IndexController extends Action
{


    public function index()
    {
//        include_once "../App/Views/index/index.phtml";

        $this->view->cars = ['Index', 'Mustang', "Ferrari"];
        $this->render("index");

    }

    public function contact()
    {
//        include_once "../App/Views/index/contact.phtml";

        $this->view->cars = ['Contact', 'Mustang', "Ferrari"];
        $this->render("contact");

    }


}