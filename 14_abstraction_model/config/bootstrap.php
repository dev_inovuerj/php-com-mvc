<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 14/11/16
 * Time: 20:23
 */


function var_dump_ret($mixed = null)
{
    ob_start();
    echo "<pre style='color: moccasin; display: block; padding: 10px; line-height: 1.7'>";
    var_dump($mixed);
    echo "</pre>";
    $content = ob_get_contents();
    ob_end_clean();

    return $content;

}

function debug($code, $title = "Debug", $asset = "")
{

    $dump = var_dump_ret($code);

    $html =
        "
        <div style=\"color:silver;background-color: black; min-width:400px; width: 100%; height: auto; border: 2px dotted yellowgreen\">
        
            <div><h4 style='text-transform: uppercase    ;padding-left:10px'>{$title}</h4></div>
            <hr>
            <div><code style='font-size:14px;font-family: sans-serif'>{$dump}</code></div>                
        </div>
        ";

    echo $html;

}


define('DSN_MYSQL', "mysql:host=localhost;dbname=test");


// Report all PHP errors
error_reporting(E_ALL);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
