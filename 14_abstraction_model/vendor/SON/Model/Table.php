<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 22:21
 */

namespace SON\Model;


abstract class Table
{
    protected $db;
    protected $table;


    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function fetchAll()
    {
        $table_name = strtolower($this->table);

        $query = "SELECT * FROM {$table_name}";

        return $this->db->query($query)->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function find($id)
    {
        $table_name = strtolower($this->table);

        $query = "SELECT * FROM {$table_name} WHERE id = :id";

        $stmt = $this->db->prepare($query);

        $stmt->bindParam(':id', $id);

        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);

    }


}