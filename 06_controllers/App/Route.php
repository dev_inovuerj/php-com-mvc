<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 19:11
 */
namespace App;

use SON\Init\Bootstrap;

class Route extends Bootstrap
{


    protected function initRoutes()
    {
        $routes['home'] = array(
            'route' => '/',
            'controller' => "indexController",
            'action' => "index"
        );

        $routes['contact'] = array(
            'route' => '/contact',
            'controller' => "indexController",
            'action' => "contact"
        );


        $this->setRoutes($routes);


    }


}