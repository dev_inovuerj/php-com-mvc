<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 20:06
 */
namespace SON\Init;

abstract class Bootstrap
{

    private $routes;

    public function __construct()
    {

        $this->initRoutes();

        $this->run($this->getUrl());

    }

    abstract protected function initRoutes();

    /**
     * Executa ACTION de um CONTROLLER associado a URL/URI
     * exemplo : http://www.app.com.br/[contact] , sendo contact action de IndexController
     * @param $url
     */
    protected function run($url)
    {
        /* Procucar URI/URL passada via param $url dentro do array $this->routes */
        array_walk($this->routes, function ($route) use ($url) {

            if ($url == $route['route']) {


                $class = "App\\Controllers\\" . ucfirst($route['controller']);

                $controller = new $class;

                $action = $route['action'];

                # $controller->$action();
                call_user_func([$controller, $action]);

            }

        });
    }

    protected function getUrl()
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }

    protected function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }

}