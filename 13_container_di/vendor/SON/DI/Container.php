<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 22:01
 */
namespace SON\DI;

use App\Conn;

class Container
{
    public static function getModel($model)
    {

        # O mesmo que :   \App\Models\Client
        $class = "\\App\\Models\\" . ucfirst($model);

        /* Retornado instancia do MODEL, passando Conexao DB*/
        return new $class(Conn::getDB());
    }
}