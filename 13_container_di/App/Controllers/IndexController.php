<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 19:47
 */
namespace App\Controllers;

use App\Conn;
use App\Models\Client;
use SON\Controller\Action;
use SON\DI\Container;

class IndexController extends Action
{

    public function index()
    {
//        $client = new Client(Conn::getDB());
        $client = Container::getModel('Client');

        $this->view->clientes = $client->fetchAll();



        $this->render("index");

    }

    public function contact()
    {
        $this->view->cars = ['Contact', 'Mustang', "Ferrari"];
        $this->render("contact", false);

    }

}