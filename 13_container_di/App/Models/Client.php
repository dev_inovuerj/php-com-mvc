<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 21:41
 */

namespace App\Models;


class Client
{
    protected $db;

    private $id, $name, $email;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function fetchAll()
    {
        $query = "SELECT * FROM clients";

        return $this->db->query($query)->fetchAll(\PDO::FETCH_ASSOC);
    }

}